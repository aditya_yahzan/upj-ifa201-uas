# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
import math

from IPython.core.display import display, HTML # u/ open file html
from pyvis.network import Network   # u/ graph

def exam001():
    print("Soal 1\nMenghitung π (pi) dan e (Euler’s Number) dengan metode Archimedes \n")
    n = round(6.0000000, 7)
    s = round(1.0000000, 7)
    s2 = round(s / 2, 7)
    a = round(math.sqrt(1 - (s2 ** 2)), 7) #sqrt akar , ** pangkat
    b = round(1 - a, 7)
    new_s = round(math.sqrt((b ** 2) + (s2 ** 2)), 7)
    p = round(n * s, 7)
    pd = round(p / 2, 7)
    # ** adalah pangkat, sqrt adalah akar kuadrat, round adalah pembulatan dengan bebas decimal place

    print("n\t\t\tS\t\t\t\tS/2\t\t\t\ta\t\t\t\tb\t\t\t\tNew S\t\t\tP\t\t\t\tP/D")
    print(n, s, "\t" + str(s2), "  " + str(a), b, new_s, p, pd, sep="\t\t\t")

    for x in range(2, 5):
        # asigment ulang
        n = round(n * 2, 7)
        s = round(new_s, 7)

        s2 = round(s / 2, 7)
        a = round(math.sqrt(1 - (s2 ** 2)), 7)
        b = round(1 - a, 7)
        new_s = round(math.sqrt((b ** 2) + (s2 ** 2)), 7)
        p = round(n * s, 7)
        pd = round(p / 2, 7)

        print(n, s, s2, a, b, new_s, p, pd, sep="\t\t")

def exam002():
    print("Soal 2\nMenghitung Euler’s Number \n")
    x = 1.0
    hasil = 0.0
    print("Interval ke 1 :", x)

    # loop ke 1 untuk operasi penjumlahan
    for y in range(1, 500):
        pembagi = 1.0
        pembagiOut = ""
        #print(y, pembagi, sep=" - ")

        # nested loop
        #   1 / 1 * 2
        # loop ke 2 untuk menampung hasil perkalian dari pembagi
        for z in range(1, y+1):
            pembagi *= z

            # u/ cetak soal / nilai yang dihitung
            if z > 1:
                pembagiOut += " x " + str(z)
            else:
                pembagiOut += str(z)
            #print("  ", z, pembagi, sep="  ")
        else:
            # cetak hasil kalkulasi
            hasil = x / pembagi
            cetak = "Interval ke " + str(y + 1) + " : " + str(x) + " / " + pembagiOut + " = "
            print(cetak, hasil)

    #print("Total = ", )

def exam004():
    net = Network()

    # lingkaran / circle / vertex
    net.add_node(0)
    net.add_node(1)
    net.add_node(2)
    net.add_node(3)
    net.add_node(4)

    # edges / garis penghubung
    net.add_edges([(0, 1), (0, 2), (0, 3), (0, 4)])

    # u/ menyimpan dan membuka file html dari pyvis
    net.show('exam004.html')

    # u/ membuka file html dari Python jika pyvis tidak bisa
    display(HTML('exam004.html'))

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    #exam001()
    #exam002()
    exam004()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
